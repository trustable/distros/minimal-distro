#!/usr/bin/python3
# Copyright (C) 2017 Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

'''test-minimal-system: Boots a disk image in QEMU and tests that it works.'''

import argparse
import asyncio
import asyncio.subprocess
import locale
import logging
import sys
import time
import subprocess


QEMU = 'qemu-system-x86_64'

FAILURE_TIMEOUT = 900   # seconds


def argument_parser():
    parser = argparse.ArgumentParser(
        description="Test that a minimal-system VM image works as expected")
    parser.add_argument("sda", help="Path to disk image file")
    return parser


async def await_line(stream, marker):
    '''Read from 'stream' until a line appears that starts with 'marker'.'''
    marker = marker
    async for line in stream:
        decoded_line = line.decode('unicode-escape')
        sys.stdout.write(decoded_line)
        if decoded_line.strip().startswith(marker):
            logging.debug("Matched line with marker: %s", decoded_line)
            return decoded_line


async def run_qemu_test(sda):
    command = [QEMU, '-drive', 'file=%s,format=raw' % sda, '-nographic', '-m', '256', '-net', 'nic', '-net', 'user,hostfwd=tcp::5555-:22']

    logging.debug("Starting process: %s", command)
    process = await asyncio.create_subprocess_exec(
        *command, stdin=asyncio.subprocess.PIPE, stdout=asyncio.subprocess.PIPE)

    success = False
    try:
        init_banner = await await_line(process.stdout, "Please press Enter to activate this console")
        print("Got BusyBox init banner:", init_banner)
        assert init_banner != None

        ssh_command = ['ssh', '-oStrictHostKeyChecking=no', '-oUserKnownHostsFile=/dev/null', '-p5555', 'root@127.0.0.1', 'uname -a']
        ssh_process = await asyncio.create_subprocess_exec(
            *ssh_command, stdin=asyncio.subprocess.PIPE, stdout=asyncio.subprocess.PIPE)
        uname = await await_line(ssh_process.stdout, "Linux")
        print("Got `uname -a` output:", uname)
        assert uname != None

        process.stdin.write('mount -t proc none /proc\n'.encode('ascii'))
        process.stdin.write('mount -o rw,remount /\n'.encode('ascii'))
        process.stdin.write('latency-measure -w register -d 60\n'.encode('ascii'))

        coco = await await_line(process.stdout, "CPU3")

        print("Latency-measure test finished\n")
        assert coco != None


        process.wait()
        ssh_command = ['scp', '-oStrictHostKeyChecking=no', '-oUserKnownHostsFile=/dev/null', '-P5555', 'root@127.0.0.1:/results.dat', './']
        ssh_process = await asyncio.create_subprocess_exec(*ssh_command, stdin=asyncio.subprocess.PIPE, stdout=asyncio.subprocess.PIPE)
        time.sleep(1)
        subprocess.check_call(['ssh', '-oStrictHostKeyChecking=no', '-oUserKnownHostsFile=/dev/null', '-p5555', 'root@127.0.0.1', 'memtester 100M 1'], stderr=subprocess.PIPE)
        print("Test successful")
        success = True
    except asyncio.CancelledError:
        # Move straight to process.kill()
        pass
    except subprocess.CalledProcessError as d:
        print (d)
        print("Test failed with return code: {:03b}" .format(d.returncode))
    finally:
        process.kill()
    await process.wait()
    return success


def fail_timeout(qemu_task):
    sys.stderr.write("Test failed as timeout of %i seconds was reached.\n" %
                     FAILURE_TIMEOUT)
    qemu_task.cancel()


def main():
    args = argument_parser().parse_args()

    loop = asyncio.get_event_loop()
    qemu_task = loop.create_task(run_qemu_test(args.sda))
    loop.call_later(FAILURE_TIMEOUT, fail_timeout, qemu_task)
    loop.run_until_complete(qemu_task)
    loop.close()

    if qemu_task.result():
        return 0
    else:
        return 1


result = main()
sys.exit(result)
